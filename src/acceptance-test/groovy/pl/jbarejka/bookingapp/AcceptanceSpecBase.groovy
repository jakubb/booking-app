package pl.jbarejka.bookingapp

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import spock.lang.Specification
import wslite.http.HTTPClient
import wslite.rest.RESTClient

@SpringBootTest(classes = [BookingApplication.class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AcceptanceSpecBase extends Specification {

    @LocalServerPort
    int randomPort

    RESTClient client

    def setup() {
        client = new RESTClient("http://localhost:${randomPort}", new HTTPClient())
    }
}

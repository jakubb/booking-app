package pl.jbarejka.bookingapp

import groovy.json.JsonSlurper
import groovy.time.TimeCategory
import wslite.rest.RESTClientException

import java.text.SimpleDateFormat

class ReservationControllerAcceptanceSpec extends AcceptanceSpecBase {

    def dateFormatter = new SimpleDateFormat("yyyy-MM-dd")
    def jsonSlurper = new JsonSlurper()

    String startDate
    String endDate

    def setup() {
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"))
        use(TimeCategory) {
            def date = new Date()
            startDate = dateFormatter.format(date + 3.day)
            endDate = dateFormatter.format(date + 5.day)
        }
    }

    def "make a reservation should throw exception if startDate is in the past"() {
        when:
        client.post(path: '/reservations') {
            json customerId: '1', roomId: '1', startDate: '2018-05-08', endDate: '2018-05-10'
        }

        then:
        def ex = thrown(RESTClientException)
        ex.getResponse().statusCode == 400
    }

    def "make a reservation should return created reservation if request proper"() {
        given: 'client created'
        def response = client.post(path: '/customers') {
            json firstName: 'John', lastName: 'Wayne'
        }
        def customerResponseJson = jsonSlurper.parseText(response.getResponse().getContentAsString())
        def customerId = customerResponseJson.id

        when:
        response = client.post(path: '/reservations') {
            json customerId: customerId, roomId: '1', startDate: startDate, endDate: endDate
        }

        then:
        response.statusCode == 201

        and:
        def responseJson = jsonSlurper.parseText(response.getResponse().getContentAsString())
        responseJson.id != null
        responseJson.customerId == customerId
        responseJson.roomId == 1
        responseJson.startDate == startDate
        responseJson.endDate == endDate
    }

    def "make a reservation already reserved room should throw exception"() {
        given: 'client created'
        def response = client.post(path: '/customers') {
            json firstName: 'John', lastName: 'Wayne'
        }
        def customerResponseJson = jsonSlurper.parseText(response.getResponse().getContentAsString())
        def customerId = customerResponseJson.id

        and: 'reservation created'
        client.post(path: '/reservations') {
            json customerId: customerId, roomId: '2', startDate: startDate, endDate: endDate
        }

        when:
        client.post(path: '/reservations') {
            json customerId: customerId, roomId: '2', startDate: startDate, endDate: endDate
        }

        then:
        def ex = thrown(RESTClientException)
        ex.getResponse().statusCode == 400
    }

    def "delete a reservation should return HTTP no content status"() {
        given: 'client created'
        def response = client.post(path: '/customers') {
            json firstName: 'John', lastName: 'Wayne'
        }
        def customerResponseJson = jsonSlurper.parseText(response.getResponse().getContentAsString())
        def customerId = customerResponseJson.id

        and: 'reservation created'
        response = client.post(path: '/reservations') {
            json customerId: customerId, roomId: '3', startDate: startDate, endDate: endDate
        }
        def reservationResponseJson = jsonSlurper.parseText(response.getResponse().getContentAsString())
        def reservationId = reservationResponseJson.id

        when:
        response = client.delete(path: "/reservations/$reservationId")

        then:
        response.statusCode == 204
    }
}

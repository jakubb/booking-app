package pl.jbarejka.bookingapp

import groovy.json.JsonSlurper
import groovy.time.TimeCategory

import java.text.SimpleDateFormat

class RoomControllerAcceptanceSpec extends AcceptanceSpecBase {

    def dateFormatter = new SimpleDateFormat("yyyy-MM-dd")
    def jsonSlurper = new JsonSlurper()

    def city = 'Warsaw'
    def startDate
    def endDate
    def minPrice = 100
    def maxPrice = 300

    def setup() {
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"))
        use(TimeCategory) {
            def date = new Date()
            startDate = dateFormatter.format(date + 3.day)
            endDate = dateFormatter.format(date + 5.day)
        }
    }

    def "get rooms should return rooms based on query params"() {
        when:
        def response = client.get(path: '/rooms', query: [city: city, minPrice: minPrice, maxPrice: maxPrice, startDate: startDate, endDate: endDate])

        then:
        response.statusCode == 200

        and:
        def responseJson = jsonSlurper.parseText(response.getResponse().getContentAsString())
        responseJson.content != null
        def content = responseJson.content
        content.city.toSet() == [city].toSet()
        content.each { assert it.price >= minPrice && it.price <= maxPrice }
    }

    def "get rooms should return only available rooms"() {
        given: 'client created'
        def response = client.post(path: '/customers') {
            json firstName: 'John', lastName: 'Wayne'
        }
        def customerResponseJson = jsonSlurper.parseText(response.getResponse().getContentAsString())
        def customerId = customerResponseJson.id

        and: 'reservation made'
        client.post(path: '/reservations') {
            json customerId: customerId, roomId: '3', startDate: startDate, endDate: endDate
        }

        when:
        response = client.get(path: '/rooms', query: [city: city, minPrice: minPrice, maxPrice: maxPrice, startDate: startDate, endDate: endDate])

        then:
        response.statusCode == 200

        and: 'should exclude already reserved room'
        def responseJson = jsonSlurper.parseText(response.getResponse().getContentAsString())
        responseJson.content != null
        def content = responseJson.content
        content.each { assert it.id != 3 }
    }
}

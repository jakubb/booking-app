package pl.jbarejka.bookingapp

import groovy.json.JsonSlurper
import wslite.rest.RESTClientException

class CustomerControllerAcceptanceSpec extends AcceptanceSpecBase {

    def jsonSlurper = new JsonSlurper()

    def "create customer should throw exception if first/last name is missing"() {
        when:
        client.post(path: '/customers')

        then:
        def ex = thrown(RESTClientException)
        ex.getResponse().statusCode == 400
    }

    def "create customer should return new customer if request proper"() {
        when:
        def response = client.post(path: '/customers') {
            json firstName: 'John', lastName: 'Wayne'
        }

        then:
        response.statusCode == 201

        and:
        def responseJson = jsonSlurper.parseText(response.getResponse().getContentAsString())
        responseJson.id != null
        responseJson.firstName == 'John'
        responseJson.lastName == 'Wayne'
    }
}

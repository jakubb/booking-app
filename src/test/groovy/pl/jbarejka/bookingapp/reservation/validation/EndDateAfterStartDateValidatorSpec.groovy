package pl.jbarejka.bookingapp.reservation.validation

import pl.jbarejka.bookingapp.reservation.ReservationDto
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import javax.validation.ConstraintValidatorContext
import java.time.LocalDate
import java.time.Month

class EndDateAfterStartDateValidatorSpec extends Specification {

    def validator = new EndDateAfterStartDateValidator()
    def constraintValidatorContext = Mock(ConstraintValidatorContext)

    @Shared
    def earlier = LocalDate.of(2018, Month.MAY, 28)
    @Shared
    def later = LocalDate.of(2018, Month.MAY, 30)

    @Unroll
    def "isValid should be #result if startDate is #startDate and endDate is #endDate"() {
        given:
        def reservationDto = new ReservationDto(startDate: startDate, endDate: endDate)

        when:
        def isValid = validator.isValid(reservationDto, constraintValidatorContext)

        then:
        isValid == result

        where:
        startDate | endDate | result
        null      | null    | false
        null      | later   | false
        earlier   | null    | false
        earlier   | earlier | false
        later     | earlier | false
        earlier   | later   | true
    }

}

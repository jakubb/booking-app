package pl.jbarejka.bookingapp.reservation

import pl.jbarejka.bookingapp.customer.Customer
import pl.jbarejka.bookingapp.customer.CustomerRepository
import pl.jbarejka.bookingapp.room.Room
import pl.jbarejka.bookingapp.room.RoomRepository
import spock.lang.Specification

import javax.persistence.EntityNotFoundException
import java.time.LocalDate
import java.time.Month

class ReservationServiceSpec extends Specification {

    def reservationRepository = Mock(ReservationRepository)
    def customerRepository = Mock(CustomerRepository)
    def roomRepository = Mock(RoomRepository)
    def reservationMapper = Mock(ReservationMapper)

    def reservationService = new ReservationService(reservationRepository, customerRepository, roomRepository, reservationMapper)

    def "create reservation should throw exception if customer does not exist"() {
        given:
        def customerId = 1
        def reservationDto = new ReservationDto(customerId: customerId)

        when:
        reservationService.createReservation(reservationDto)

        then:
        1 * customerRepository.findById(customerId) >> Optional.empty()
        thrown(IllegalArgumentException)
    }

    def "create reservation should throw exception if room does not exist"() {
        given:
        def customerId = 1
        def roomId = 1
        def reservationDto = new ReservationDto(customerId: customerId, roomId: roomId)

        when:
        reservationService.createReservation(reservationDto)

        then:
        1 * customerRepository.findById(customerId) >> Optional.of(new Customer())
        1 * roomRepository.findById(roomId) >> Optional.empty()
        thrown(IllegalArgumentException)
    }

    def "create reservation should throw exception if room is already reserved"() {
        given:
        def customerId = 1
        def roomId = 1
        def startDate = LocalDate.of(2018, Month.MAY, 27)
        def endDate = LocalDate.of(2018, Month.MAY, 25)
        def reservationDto = new ReservationDto(customerId: customerId, roomId: roomId, startDate: startDate, endDate: endDate)

        when:
        reservationService.createReservation(reservationDto)

        then:
        1 * customerRepository.findById(customerId) >> Optional.of(new Customer())
        1 * roomRepository.findById(roomId) >> Optional.of(new Room())
        1 * reservationRepository.existsByRoomIdAndStartDateBeforeAndEndDateAfter(roomId, endDate, startDate) >> true
        thrown(IllegalArgumentException)
    }

    def "create reservation should pass if room is not reserved yet"() {
        given:
        def customerId = 1
        def roomId = 1
        def startDate = LocalDate.of(2018, Month.MAY, 27)
        def endDate = LocalDate.of(2018, Month.MAY, 25)
        def reservationDto = new ReservationDto(customerId: customerId, roomId: roomId, startDate: startDate, endDate: endDate)

        when:
        reservationService.createReservation(reservationDto)

        then:
        1 * customerRepository.findById(customerId) >> Optional.of(new Customer())
        1 * roomRepository.findById(roomId) >> Optional.of(new Room())
        1 * reservationRepository.existsByRoomIdAndStartDateBeforeAndEndDateAfter(roomId, endDate, startDate) >> false
        1 * reservationRepository.save(_)
    }

    def "delete reservation should throw exception if reservation does not exist"() {
        given:
        def reservationId = 1

        when:
        reservationService.deleteReservation(reservationId)

        then:
        1 * reservationRepository.findById(reservationId) >> Optional.empty()
        thrown(EntityNotFoundException)
    }

    def "delete reservation should throw exception if it's past reservation"() {
        given:
        def reservationId = 1

        when:
        reservationService.deleteReservation(reservationId)

        then:
        1 * reservationRepository.findById(reservationId) >> Optional.of(new Reservation(startDate: LocalDate.of(2018, Month.MAY, 10)))
        thrown(IllegalArgumentException)
    }

    def "delete reservation should succeed if it's future reservation"() {
        given:
        def reservationId = 1

        when:
        reservationService.deleteReservation(reservationId)

        then:
        1 * reservationRepository.findById(reservationId) >> Optional.of(new Reservation(startDate: LocalDate.now().plusDays(1)))
        1 * reservationRepository.deleteById(reservationId)
    }
}

package pl.jbarejka.bookingapp.reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/reservations")
public class ReservationController {

  private final ReservationService reservationService;

  @Autowired
  public ReservationController(ReservationService reservationService) {
    this.reservationService = reservationService;
  }

  @GetMapping
  public Page<ReservationDto> findAll(@RequestParam Long customerId, Pageable pageable) {
    return reservationService.findAll(customerId, pageable);
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public ReservationDto createReservation(@RequestBody @Validated ReservationDto reservationDto) {
    return reservationService.createReservation(reservationDto);
  }

  @DeleteMapping("/{reservationId}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteReservation(@PathVariable Long reservationId) {
    reservationService.deleteReservation(reservationId);
  }
}

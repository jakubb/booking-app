package pl.jbarejka.bookingapp.reservation.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import pl.jbarejka.bookingapp.reservation.ReservationDto;

public class EndDateAfterStartDateValidator implements ConstraintValidator<EndDateAfterStartDate, ReservationDto> {

  @Override
  public final void initialize(final EndDateAfterStartDate annotation) {

  }

  @Override
  public final boolean isValid(final ReservationDto reservationDto,
      final ConstraintValidatorContext context) {
    if (reservationDto.getEndDate() == null || reservationDto.getStartDate() == null) {
      return false;
    }
    return reservationDto.getEndDate().isAfter(reservationDto.getStartDate());
  }
}
package pl.jbarejka.bookingapp.reservation;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import lombok.Data;
import pl.jbarejka.bookingapp.reservation.validation.EndDateAfterStartDate;

@Data
@EndDateAfterStartDate
public class ReservationDto {

  private Long id;

  @NotNull
  private Long customerId;

  private String customerFirstName;

  private String customerLastName;

  @NotNull
  private Long roomId;

  private String hotelName;

  private String city;

  @NotNull
  @Future
  private LocalDate startDate;

  @NotNull
  @Future
  private LocalDate endDate;

  private BigDecimal totalPrice;
}

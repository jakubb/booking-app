package pl.jbarejka.bookingapp.reservation;

import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

  @EntityGraph(attributePaths = {"customer", "room", "room.hotel"}, type = EntityGraph.EntityGraphType.LOAD)
  Page<Reservation> findByCustomerId(Long customerId, Pageable pageable);

  boolean existsByRoomIdAndStartDateBeforeAndEndDateAfter(Long roomId, LocalDate givenEndDate, LocalDate givenStartDate);
}

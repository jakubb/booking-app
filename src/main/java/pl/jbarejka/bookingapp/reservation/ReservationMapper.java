package pl.jbarejka.bookingapp.reservation;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ReservationMapper {

  @Mapping(source = "customer.id", target = "customerId")
  @Mapping(source = "customer.firstName", target = "customerFirstName")
  @Mapping(source = "customer.lastName", target = "customerLastName")
  @Mapping(source = "room.id", target = "roomId")
  @Mapping(source = "room.hotel.name", target = "hotelName")
  @Mapping(source = "room.hotel.city", target = "city")
  ReservationDto toDto(Reservation source);

  @Mapping(source = "customerId", target = "customer.id")
  @Mapping(source = "roomId", target = "room.id")
  Reservation toEntity(ReservationDto source);
}

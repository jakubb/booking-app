package pl.jbarejka.bookingapp.reservation;

import java.time.LocalDate;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import javax.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.jbarejka.bookingapp.customer.CustomerRepository;
import pl.jbarejka.bookingapp.room.RoomRepository;

@Service
@Transactional
public class ReservationService {

  private final ReservationRepository reservationRepository;
  private final CustomerRepository customerRepository;
  private final RoomRepository roomRepository;
  private final ReservationMapper mapper;

  private ConcurrentMap<Long, Long> locks = new ConcurrentHashMap<>();

  @Autowired
  public ReservationService(ReservationRepository reservationRepository,
      CustomerRepository customerRepository,
      RoomRepository roomRepository,
      ReservationMapper mapper) {
    this.reservationRepository = reservationRepository;
    this.customerRepository = customerRepository;
    this.roomRepository = roomRepository;
    this.mapper = mapper;
  }

  @Transactional(readOnly = true)
  public Page<ReservationDto> findAll(Long customerId, Pageable pageable) {
    return reservationRepository.findByCustomerId(customerId, pageable)
        .map(mapper::toDto);
  }

  public ReservationDto createReservation(ReservationDto reservationDto) {
    customerRepository.findById(reservationDto.getCustomerId())
        .orElseThrow(() -> new IllegalArgumentException("Customer does not exist!"));
    roomRepository.findById(reservationDto.getRoomId())
        .orElseThrow(() -> new IllegalArgumentException("Room does not exist!"));

    synchronized (getLock(reservationDto.getRoomId())) {
      boolean isAlreadyReserved = reservationRepository.existsByRoomIdAndStartDateBeforeAndEndDateAfter(
          reservationDto.getRoomId(),
          reservationDto.getEndDate(),
          reservationDto.getStartDate());
      if (isAlreadyReserved) {
        throw new IllegalArgumentException("Room has been already reserved for this period of time!");
      }

      Reservation savedReservation = reservationRepository.save(mapper.toEntity(reservationDto));
      return mapper.toDto(savedReservation);
    }
  }

  public void deleteReservation(Long reservationId) {
    Reservation reservation = reservationRepository.findById(reservationId)
        .orElseThrow(() -> new EntityNotFoundException("Reservation does not exist!"));

    if (reservation.getStartDate().isBefore(LocalDate.now())) {
      throw new IllegalArgumentException("You cannot remove past reservation!");
    }

    reservationRepository.deleteById(reservationId);
  }

  private Object getLock(Long id) {
    locks.putIfAbsent(id, id);
    return locks.get(id);
  }
}

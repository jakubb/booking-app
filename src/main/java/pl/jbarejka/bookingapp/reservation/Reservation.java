package pl.jbarejka.bookingapp.reservation;

import static java.time.temporal.ChronoUnit.DAYS;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import pl.jbarejka.bookingapp.customer.Customer;
import pl.jbarejka.bookingapp.room.Room;

@Entity
@Table(indexes = {@Index(name = "reservation_dates_idx", columnList = "startDate, endDate")})
@Data
public class Reservation {

  @Id
  @GeneratedValue
  @Column(name = "reservation_id")
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "customer_id", nullable = false)
  private Customer customer;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "room_id", nullable = false)
  private Room room;

  @Column(nullable = false)
  private LocalDate startDate;

  @Column(nullable = false)
  private LocalDate endDate;

  public BigDecimal getTotalPrice() {
    return Optional.ofNullable(this.room.getPrice())
        .map(price -> price.multiply(new BigDecimal(DAYS.between(this.startDate, this.endDate))))
        .orElse(null);
  }
}

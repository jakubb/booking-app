package pl.jbarejka.bookingapp.hotel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(indexes = {@Index(name = "hotel_city_idx", columnList = "city")})
@Data
public class Hotel {

  @Id
  @GeneratedValue
  @Column(name = "hotel_id")
  private Long id;

  @Column(nullable = false)
  private String name;

  @Column(nullable = false)
  private String city;
}

package pl.jbarejka.bookingapp.common.validation;

import lombok.Data;

@Data
class ApiErrorResponse {

  private final int status;

  private final String message;

  private final String detail;
}

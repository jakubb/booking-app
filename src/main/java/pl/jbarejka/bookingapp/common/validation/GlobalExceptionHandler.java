package pl.jbarejka.bookingapp.common.validation;

import java.util.ConcurrentModificationException;
import javax.persistence.EntityNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ApiErrorResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
    logger.warn("Validation error", ex);
    return new ApiErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(),
        ex.getBindingResult().getAllErrors().toString());
  }

  @ExceptionHandler({IllegalArgumentException.class, HttpMessageNotReadableException.class,
      MissingServletRequestParameterException.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ApiErrorResponse handleIllegalArgumentException(Exception ex) {
    logger.warn("Validation error", ex);
    return new ApiErrorResponse(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), null);
  }

  @ExceptionHandler({DataIntegrityViolationException.class, ConcurrentModificationException.class})
  @ResponseStatus(value = HttpStatus.CONFLICT)
  public ApiErrorResponse handleDataIntegrityViolation(Exception ex) {
    logger.warn("Data integrity violation", ex);
    return new ApiErrorResponse(HttpStatus.CONFLICT.value(), ex.getLocalizedMessage(), null);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public ApiErrorResponse handleEntityNotFoundException(EntityNotFoundException ex) {
    logger.warn("Entity not found", ex);
    return new ApiErrorResponse(HttpStatus.NOT_FOUND.value(), ex.getLocalizedMessage(), null);
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ApiErrorResponse handleGeneralException(Exception ex) {
    logger.error("Internal server error", ex);
    return new ApiErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), ex.getLocalizedMessage(), null);
  }
}
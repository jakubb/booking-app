package pl.jbarejka.bookingapp.customer;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CustomerDto {

  private Long id;

  @NotNull
  private String firstName;

  @NotNull
  private String lastName;
}

package pl.jbarejka.bookingapp.customer;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

  CustomerDto toDto(Customer source);

  @Mapping(target = "reservations", expression = "java(new java.util.ArrayList<>())")
  Customer toEntity(CustomerDto source);
}

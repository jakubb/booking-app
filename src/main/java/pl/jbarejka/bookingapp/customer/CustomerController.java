package pl.jbarejka.bookingapp.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customers")
@Transactional
public class CustomerController {

  private final CustomerRepository customerRepository;
  private final CustomerMapper mapper;

  @Autowired
  public CustomerController(CustomerRepository customerRepository, CustomerMapper mapper) {
    this.customerRepository = customerRepository;
    this.mapper = mapper;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public CustomerDto createCustomer(@RequestBody @Validated CustomerDto customerDto) {
    Customer customer = mapper.toEntity(customerDto);
    Customer savedCustomer = customerRepository.save(customer);
    return mapper.toDto(savedCustomer);
  }
}

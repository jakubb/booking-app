package pl.jbarejka.bookingapp.customer;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Data;
import pl.jbarejka.bookingapp.reservation.Reservation;

@Entity
@Data
public class Customer {

  @Id
  @GeneratedValue
  @Column(name = "customer_id")
  private Long id;

  @Column(nullable = false)
  private String firstName;

  @Column(nullable = false)
  private String lastName;

  @OneToMany(mappedBy = "customer")
  private List<Reservation> reservations;
}

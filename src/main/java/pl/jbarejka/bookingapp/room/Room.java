package pl.jbarejka.bookingapp.room;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import pl.jbarejka.bookingapp.hotel.Hotel;
import pl.jbarejka.bookingapp.reservation.Reservation;

@Entity
@Table(indexes = {@Index(name = "room_price_idx", columnList = "price")})
@Data
public class Room {

  @Id
  @GeneratedValue
  @Column(name = "room_id")
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "hotel_id", nullable = false)
  private Hotel hotel;

  @Column(nullable = false)
  private BigDecimal price;

  @OneToMany(mappedBy = "room")
  private List<Reservation> reservations;
}

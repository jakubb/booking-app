package pl.jbarejka.bookingapp.room;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface RoomMapper {

  @Mapping(source = "hotel.name", target = "hotelName")
  @Mapping(source = "hotel.city", target = "city")
  RoomDto toDto(Room source);
}

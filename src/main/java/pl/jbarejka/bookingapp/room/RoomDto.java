package pl.jbarejka.bookingapp.room;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class RoomDto {

  private Long id;

  private String hotelName;

  private String city;

  private BigDecimal price;
}

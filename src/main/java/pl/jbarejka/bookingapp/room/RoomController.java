package pl.jbarejka.bookingapp.room;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPAExpressions;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.jbarejka.bookingapp.reservation.QReservation;

@RestController
@RequestMapping("/rooms")
@Transactional(readOnly = true)
public class RoomController {

  private final RoomRepository roomRepository;
  private final RoomMapper roomMapper;

  @Autowired
  public RoomController(RoomRepository roomRepository, RoomMapper roomMapper) {
    this.roomRepository = roomRepository;
    this.roomMapper = roomMapper;
  }

  @GetMapping
  public Page<RoomDto> findAll(@RequestParam(required = false) String city,
      @RequestParam(required = false) BigDecimal minPrice,
      @RequestParam(required = false) BigDecimal maxPrice,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
      @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
      Pageable pageable) {

    BooleanBuilder booleanBuilder = new BooleanBuilder();

    if (city != null) {
      booleanBuilder.and(QRoom.room.hotel.city.eq(city));
    }

    if (minPrice != null) {
      booleanBuilder.and(QRoom.room.price.goe(minPrice));
    }

    if (maxPrice != null) {
      booleanBuilder.and(QRoom.room.price.loe(maxPrice));
    }

    if (startDate != null && endDate != null) {
      booleanBuilder.and(QRoom.room.id.notIn(
          JPAExpressions.select(QReservation.reservation.room.id)
              .from(QReservation.reservation)
              .where(QReservation.reservation.startDate.lt(endDate).and(QReservation.reservation.endDate.gt(startDate))))
      );
    }

    Predicate predicate = booleanBuilder.getValue();

    return roomRepository.findAll(predicate, pageable)
        .map(roomMapper::toDto);
  }
}

package pl.jbarejka.bookingapp.room;

import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long>, QuerydslPredicateExecutor<Room> {

  @EntityGraph(attributePaths = {"hotel"}, type = EntityGraph.EntityGraphType.LOAD)
  Page<Room> findAll(Predicate predicate, Pageable pageable);
}

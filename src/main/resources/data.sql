insert into hotel(hotel_id, `name`, city) values (1, 'Hilton', 'Warsaw');
insert into hotel(hotel_id, `name`, city) values (2, 'Mercury', 'Prague');
insert into hotel(hotel_id, `name`, city) values (3, 'Marriott', 'Warsaw');
insert into hotel(hotel_id, `name`, city) values (4, 'Novotel', 'Warsaw');

insert into room(room_id, hotel_id, price) values (1, 1, 100);
insert into room(room_id, hotel_id, price) values (2, 1, 150);
insert into room(room_id, hotel_id, price) values (3, 1, 250);
insert into room(room_id, hotel_id, price) values (4, 1, 350);
insert into room(room_id, hotel_id, price) values (5, 2, 50);
insert into room(room_id, hotel_id, price) values (6, 2, 550);
insert into room(room_id, hotel_id, price) values (7, 3, 250);
insert into room(room_id, hotel_id, price) values (8, 4, 300);

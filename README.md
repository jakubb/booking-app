# Booking App

## Building and Running the Application

The project uses gradle to build the Spring Boot application. To build and run the application, invoke:

```
./gradlew bootRun
```
The in-memory H2 database will be populated with some data during a boot automatically.

To run acceptation tests, invoke:
```
./gradlew acceptanceTest
```